<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Collection;

use ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Extension\Base as BaseExtension;

class Extension
{
    /**
     * Stores extensions grouped by type and identifier.
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * @param iterable $extensions List of template extensions
     */
    public function __construct(iterable $extensions)
    {
        foreach ($extensions as $extension) {
            $this->registerExtension($extension);
        }
    }

    /**
     * Registers an template extension.
     */
    protected function registerExtension(BaseExtension $extension): void
    {
        $type = $extension->getType();
        $identifier = $extension->getIdentifier();

        if (isset($this->extensions[$type]) === false) {
            $this->extensions[$type] = [];
        }

        if (isset($this->extensions[$type][$identifier]) === false) {
            $this->extensions[$type][$identifier] = [];
        }
        $this->extensions[$type][$identifier][] = $extension;
    }

    /**
     * Returns additional parameters (which will be available in the view).
     *
     * @param string $type Type
     * @param string $identifier Identifier
     * @param array $params Current set of parameters
     */
    public function extend(string $type, string $identifier, array $params): array
    {
        $extensions = $this->getExtensions($type, $identifier);
        $this->normalizeParams($params);

        foreach ($extensions as $extension) {
            $params = array_merge_recursive($params, $extension->extend($params));
        }


        return $params;
    }

    /**
     * Retrieves list of extensions for template with specified type and identifier.
     *
     * @param string $type Type
     * @param string $identifier Identifier
     *
     * @return array<\ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Extension\Base>
     */
    protected function getExtensions(string $type, string $identifier): array
    {
        if (isset($this->extensions[$type]) === false) {
            return [];
        }

        return $this->extensions[$type][$identifier] ?? [];
    }

    /**
     * Converts empty array to string, as core converting <ezvalue key="attr"/> to 'attr' => [].
     */
    protected function normalizeParams(array &$params): void
    {
        foreach ($params as $k => $v) {
            if ($v === []) {
                $params[$k] = '';
            }
        }
    }
}
