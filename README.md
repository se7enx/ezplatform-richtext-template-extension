# eZ Platform RichText Template Extension

eZ Platform bundle which allows implementing custom logic for custom tags/styles.

## Installation

1. Run composer require:
    ```
    composer require contextualcode/ezplatform-richtext-template-extension
    ```

## Usage

Lets say you have `place_info` custom tag. Which has `place_location_id` paramater. And `place_info.html.twig` template is used to render it. 

Configuration for this custom tag would similar to:
```yaml
ezrichtext:
    custom_tags:
        place_info:
            template: '@ezdesign/field_type/ezrichtext/custom_tag/place_info.html.twig'
            icon: '/bundles/app/img/icons.svg#place_info'
            is_inline: false
            attributes:
                place_location_id:
                    type: 'number'
                    required: true
```

Right now `params.place_location_id` variable is available in `place_info.html.twig` template. And let's say we need to have place `Location` object in that template. In order to do so, we need to follow simples steps.

### 1. Create custom tag extension service class

It need to extend `ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Extension\Base` and `extend` method need to be implemented. It will return addtional paramters, which will be available in custom tag template.
We will inject `ezpublish.api.service.location` service, and it will be used to load place `Location`. Also custom tage name need to be passed in parent constructor:

```php
<?php

namespace AcmeBundle\CustomTagExtension;

use Exception;
use eZ\Publish\API\Repository\LocationService;
use ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Extension\Base;

class Location extends Base
{
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;

    /**
     * @param \eZ\Publish\API\Repository\LocationService
     * @param string $identifier Identifier
     * @param string $type Type: tag or style
     */
    public function __construct(LocationService $locationService, string $identifier, string $type)
    {
        $this->locationService = $locationService;

        parent::__construct($identifier, $type);
    }

    /**
     * Returns additional parameters which will be available in the view.
     * You can use injected services here to get them.
     *
     * @param array $params Current set of parameters
     *
     * @return array
     */
    public function extend(?array $params): array
    {
        if (isset($params['place_location_id']) === false) {
            return [];
        }

        try {
            $location = $this->locationService->loadLocation($params['place_location_id']);
        } catch (Exception $e) {
            return [];
        }

        return ['place_location' => $location];
    }
}
```

### 2. Define custom tag extension service

Service need to have `ezpublish.ezrichtext.converter.output.template.extension` tag. So its definition will be similar to:
```yaml
services:
    ezrichtext.converter.template.extensions.accordion:
        class: AcmeBundle\CustomTagExtension\Location
        tags: [ezpublish.ezrichtext.converter.output.template.extension]
        arguments:
            - '@ezpublish.api.service.location'
            - 'location'
            - 'tag'
```

### 3. Use new parameter in template

That is all, now you have `params.place_location` variable in `place_info.html.twig` template.